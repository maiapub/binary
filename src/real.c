/* gcc -O -std=gnu99 -Wall -o real real.c */

#include <stdio.h>   /* fprintf() */


static void findIntRange() {
	int start = (1 << 24) - 5;
	float f = start;
	for (int i = 0;  i < 10;  ++i) {
		printf("%.1f\n", f);
		f += 1;
	}
	f = -start;
	for (int i = 0;  i < 10;  ++i) {
		printf("%.1f\n", f);
		f -= 1;
	}

	long begin = (1L << 53) - 5;
	double d = begin;
	for (int i = 0;  i < 10;  ++i) {
		printf("%.1f\n", d);
		d += 1;
	}
	d = -begin;
	for (int i = 0;  i < 10;  ++i) {
		printf("%.1f\n", d);
		d -= 1;
	}
}


static void showRoundingError() {
	float f = 0.1f;
	float sum = 0;
	for (int i = 0;  i < 10;  ++i) {
		sum += f;
	}
	printf("sum = %.15f\n", sum);
}


static void addNoChange() {
	float big = (1 << 22);
	float small = 0.25f;
	float sum = big + small;
	printf("big %.2f  small %.2f  sum %.2f\n", big, small, sum);
	big = (1 << 22) + 1;
	float diff = big - small;
	printf("big %.2f  small %.2f  diff %.2f\n", big, small, diff);
	float data[] = {
		4194305.0F, 4194305.1F, 4194305.2F, 4194305.3F,
		4194305.4F, 4194305.5F, 4194305.6F, 4194305.7F,
		4194305.8F, 4194305.9F, 4194306.0F
	};
	for (int i = 0;  i < 11;  ++i) {
		printf("%d %.2f\n", i, data[i]);
	}
}


int main(int argc, const char *argv[]) {
	findIntRange();
	showRoundingError();
	addNoChange();
	return 0;
}
