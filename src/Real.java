import java.math.BigDecimal;
import java.math.BigInteger;


public class Real {
	public static void main(String[] args) {
		//example2();
		//example3();
		//example4();
		//example5();
		//example6();
		//example7();
		//example8();
		//example9();
		//example10();
		example11();
		example12();
		//revQues3();
	}


	private static void example2() {
		println("Example 2");
		float f = 14.625F;
		println(toBin(f));
	}


	private static void example3() {
		println("Example 3");
		{
			BigDecimal x = new BigDecimal("75.25");
			BigDecimal y = new BigDecimal("15.875");
			BigDecimal z = x.add(y);
			println(toBin(x));
			println(toBin(y));
			println(toBin(z));
		}
		{
			float x = 75.25F;
			float y = 15.875F;
			float z = x + y;
			println(toBin(x));
			println(toBin(y));
			println(toBin(z));
		}
	}


	private static void example4() {
		println("Example 4");
		{
			BigInteger incr = new BigInteger("10000000000000000");
			BigInteger counter = incr;
			BigInteger end = new BigInteger("90000000000000000000000");
			int max = 0;
			while (counter.compareTo(end) < 0) {
				int lsb = counter.getLowestSetBit();
				int tz = trailingDecZeros(counter);
				if (lsb >= 34 && (lsb + tz) >= max) {
					max = lsb + tz;
					println(toBin(counter));
				}
				counter = counter.add(incr);
			}

			float best = 2.097152e22F;
			println(toBin(best));
		}

		{
			BigDecimal incr = BigDecimal.ONE.divide(new BigDecimal(1L << 40));
			BigDecimal counter = incr;
			BigDecimal end = BigDecimal.ONE.divide(new BigDecimal(1L << 27));
			//BigDecimal temp = end;
			//end = temp.add(temp).add(temp);
			println(toBin(incr));
			println(toBin(end));
			int min = Integer.MAX_VALUE;
			while (counter.compareTo(end) <= 0) {
				int td = trailingDecDigits(counter);
				if (td < min) {
					min = td;
					println(toBin(counter));
				}
				counter = counter.add(incr);
			}
		}
	}


	private static void example6() {
		println("Example 6");
		int a = 0b01100100100011100001101111001010;
		int b = 0b11000101111000100000000000000000;
		int c = 0b00001010000001100110100100000000;
		float x = Float.intBitsToFloat(a);
		float y = Float.intBitsToFloat(b);
		float z = Float.intBitsToFloat(c);
		println(toBin(x));
		println(toBin(y));
		println(toBin(z));
	}


	private static void example7() {
		println("Example 7");
		float x = 0.1f;
		float y = 0.2f;
		float z = x + y;
		println(x + " + " + y + " = " + z);
		println(toBin(x));
		println(toBin(y));
		println(toBin(z));
		println(toBin(0.3f));
	}


	private static void example8() {
		println("Example 8");
		float singles[] = {
				Float.MIN_VALUE,
				Float.MIN_NORMAL,
				Float.MAX_VALUE
		};
		for (float f : singles) {
			println(toBin(f));
		}
	}


	private static void example9() {
		BigInteger half  = BigInteger.ONE.shiftLeft(11);
		BigInteger singl = BigInteger.ONE.shiftLeft(24);
		BigInteger doubl = BigInteger.ONE.shiftLeft(53);
		BigInteger quad  = BigInteger.ONE.shiftLeft(113);
		println("half " + half);
		println("single " + singl);
		println("double " + doubl);
		println("quad " + quad);
	}


	private static void example10() {
		println("Example 10");
		float f = -7604.32F;
		println(toBin(f));
		f = 63084;
		println(toBin(f));
	}


	private static void example11() {
		println("Example 11");
		float[] values = { 0.0001f, 0.25f, 0.5f, 1f, 16f, 17f };
		for (float f : values) {
			println(toBin(f));
			println("  " + toBin(Math.ulp(f)));
			println("  " + toBin(f + Math.ulp(f)));
		}
		float f = 0.1f;
		float sum = 0;
		for (int i = 0;  i < 10;  ++i) {
			sum += f;
		}
		println("sum = " + sum);
		float f1 = 1f;
		float f2 = 1.00001f;
		float ulp = Math.ulp(Math.max(Math.abs(f1), Math.abs(f2)));
		if (Math.abs(f1 - f2) <= ulp * 2) {
			println(f1 + " and " + f2 + " are within 2 * " + ulp +
					" so they are close enough to be considered equal.");
		}
		else {
			println(f1 + " and " + f2 + " are not within 2 * " + ulp +
					" so they are considered unequal.");
		}
	}


	private static void example12() {
		println("Example 12");
		float[] data = {
			4194305.0F, 4194305.1F, 4194305.2F, 4194305.3F,
			4194305.4F, 4194305.5F, 4194305.6F, 4194305.7F,
			4194305.8F, 4194305.9F
		};
		for (int i = 0;  i < data.length;  ++i) {
			println(i + " " + data[i]);
		}
	}

	private static void revQues3() {
		println("Review Question 3");
		float f = -872.15f;
		int i = Float.floatToRawIntBits(f);
		println(toBin(f));
		println(Long.toString((long)i & 0xffffffffL, 16));
	}


	/*
	private static int leadingBinZeros(BigDecimal big) {
		MathContext ctx = new MathContext(Integer.MAX_VALUE, RoundingMode.DOWN);
		BigDecimal frac = big.subtract(big.round(ctx));
		String s = frac.toString(2);
		int i;
		for (i = 0;  i < s.length();  ++i) {
			if (s.charAt(i) != '0') {
				break;
			}
		}
		return i;
	}
	 */


	private static int trailingDecDigits(BigDecimal big) {
		String s = big.toPlainString();
		int dot = s.indexOf('.');
		s = s.substring(dot + 1);
		int i;
		for (i = 0;  i < s.length();  ++i) {
			if (s.charAt(i) != '0') {
				break;
			}
		}
		s = s.substring(i);
		for (i = s.length() - 1;  i >= 0;  --i) {
			if (s.charAt(i) != '0') {
				break;
			}
		}
		return i + 1;
	}


	private static int leadingDecZeros(BigDecimal big) {
		String s = big.toPlainString();
		int dot = s.indexOf('.');
		s = s.substring(dot + 1);
		int i;
		for (i = 0;  i < s.length();  ++i) {
			if (s.charAt(i) != '0') {
				break;
			}
		}
		return i;
	}


	private static int trailingDecZeros(BigInteger big) {
		String s = big.toString();
		int i;
		for (i = s.length() - 1;  i >= 0;  --i) {
			if (s.charAt(i) != '0') {
				break;
			}
		}
		return s.length() - i - 1;
	}


	private static String toBin(BigDecimal big) {
		StringBuilder sb = new StringBuilder(big.toPlainString());
		//sb.append(": ").append(trailingDecDigits(big)).append("; ");
		sb.append("; ");

		int sign = big.signum();
		BigInteger w = big.toBigInteger();
		long f = Double.doubleToRawLongBits(big.subtract(new BigDecimal(w)).doubleValue());

		sign = sign == 1 ? 0 : (sign == -1 ? 1 : 0);
		sb.append(sign).append(',');

		byte[] a = w.toByteArray();
		for (int i = 0;  i < a.length;  ++i) {
			String s = Integer.toString((int)a[i] & 0xff, 2);
			s = "00000000".substring(s.length()) + s;
			sb.append(' ').append(s.substring(0, 4))
			.append(' ').append(s.substring(4));
		}

		f = significand(f);
		String s = Long.toString(f, 2);
		s = "00000000000000000000000000000000000000000000000000000"
				.substring(s.length()) + s;
		sb.append(" . ").append(s.substring(0, 1));
		for (int i = 1;  i < 53;  i += 4) {
			sb.append(' ').append(s.substring(i, i + 4));
		}

		/*
		byte[] a = big.toByteArray();
		for (int i = 0;  i < a.length;  ++i) {
			String s = Integer.toString((int)a[i] & 0xff, 2);
			s = "00000000".substring(s.length()) + s;
			sb.append(' ').append(s.substring(0, 4))
				.append(' ').append(s.substring(4));
		}
		sb.append(": ").append(big.bitCount()).append(", ").append(big.getLowestSetBit());
		 */
		return sb.toString();
	}


	private static String toBin(BigInteger big) {
		StringBuilder sb = new StringBuilder(big.toString());
		sb.append(": ").append(trailingDecZeros(big)).append("; ");
		byte[] a = big.toByteArray();
		for (int i = 0;  i < a.length;  ++i) {
			String s = Integer.toString((int)a[i] & 0xff, 2);
			s = "00000000".substring(s.length()) + s;
			sb.append(' ').append(s.substring(0, 4))
			.append(' ').append(s.substring(4));
		}
		sb.append(": ").append(big.bitCount()).append(", ").append(big.getLowestSetBit());
		return sb.toString();
	}

	private static String toBin(double d) {
		StringBuilder sb = new StringBuilder();
		sb.append(d).append('\t');

		long n = Double.doubleToRawLongBits(d);
		sb.append(Integer.toString(sign(n)));

		String exp = Integer.toString(exponent(n), 2);
		exp = "00000000000".substring(exp.length()) + exp;
		sb.append(", ").append(exp.substring(0, 3));
		for (int i = 3;  i < 11;  i += 4) {
			sb.append(' ').append(exp.substring(i, i + 4));
		}

		String frac = Long.toString(fraction(n), 2);
		frac = "0000000000000000000000000000000000000000000000000000"
				.substring(frac.length()) + frac;
		sb.append(',');
		for (int i = 0;  i < 52;  i += 4) {
			sb.append(' ').append(frac.substring(i, i + 4));
		}

		return sb.toString();
	}

	private static String toBin(float f) {
		StringBuilder sb = new StringBuilder();
		sb.append(f).append('\t');

		int n = Float.floatToRawIntBits(f);
		sb.append(Integer.toString(sign(n))).append(',');

		String exp = Integer.toString(exponent(n), 2);
		exp = "00000000".substring(exp.length()) + exp;
		for (int i = 0;  i < 8;  i += 4) {
			sb.append(' ').append(exp.substring(i, i + 4));
		}

		String frac = Integer.toString(fraction(n), 2);
		frac = "00000000000000000000000".substring(frac.length()) + frac;
		sb.append(", ").append(frac.substring(0, 3));
		for (int i = 3;  i < 23;  i += 4) {
			sb.append(' ').append(frac.substring(i, i + 4));
		}

		return sb.toString();
	}

	private static int sign(long d) {
		int signShift = 63;
		return (int)(d >>> signShift);
	}

	private static int exponent(long d) {
		long expMask = 0x7ff0000000000000L;
		int expShift = 52;
		return (int)((d & expMask) >> expShift);
	}

	private static long significand(long d) {
		int e = exponent(d);
		long f = fraction(d);
		long n;
		if (f == 0) {
			n = 1L << (52 + (e - 1023) + 1);
		}
		else {
			f >>>= -(e - 1023) - 1;
			n = (Long.highestOneBit(f) << 1) | f;
		}
		return n;
	}

	private static long fraction(long d) {
		long sigMask = 0x000fffffffffffffL;
		return d & sigMask;
	}

	private static int sign(int f) {
		int signShift = 31;
		return f >>> signShift;
	}

	private static int exponent(int f) {
		int expMask = 0x7f800000;
		int expShift = 23;
		return (f & expMask) >> expShift;
	}

	private static int significand(int f) {
		f = fraction(f);
		int n = (f == 0 ? 1 : (Integer.highestOneBit(f) << 1) | f);
		return n;
	}

	private static int fraction(int f) {
		int sigMask  = 0x007fffff;
		return f & sigMask;
	}


	private static void println(String s) {
		System.out.println(s);
	}
}
