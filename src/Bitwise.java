import java.util.Random;


public class Bitwise {
	public static void main(String[] args) {
		/*
		example1();
		example2();
		example3();
		example4();
		example5();
		example6();
		example7();
		example8();
		example9();
		example10();
		example11();
		example12();
		example13();
		*/
		example14();
		example15();
		example16();
//		example17();
	}

	private static void example1() {
		print("Example 1");
		int x = 58;
		int pos = x << 2;
		int z = -42;
		int neg = z << 2;
		print("x", x);
		print("pos", pos);
		print("z", z);
		print("neg", neg);
	}

	private static void example2() {
		print("Example 2");
		int x = 58;
		int pos = x >>> 2;
		int z = -42;
		int neg = z >>> 2;
		print("x", x);
		print("pos", pos);
		print("z", z);
		print("neg", neg);
	}

	private static void example3() {
		print("Example 3");
		int x = 58;
		int pos = x >> 2;
		print("x", x);
		print("pos", pos);
	}

	private static void example4() {
		print("Example 4");
		int w = -40;
		int x = -41;
		int y = -42;
		int z = -43;
		int srsw = w >> 2;
		int srsx = x >> 2;
		int srsy = y >> 2;
		int srsz = z >> 2;
		print("w", w);
		print("x", x);
		print("y", y);
		print("z", z);
		print("srsw", srsw);
		print("srsx", srsx);
		print("srsy", srsy);
		print("srsz", srsz);
	}

	private static void example5() {
		print("Example 5");
		int x = -43;
		int div = x / (int)Math.pow(2, 2);
		int flr = (int)Math.floor(x / Math.pow(2, 2));
		print("x", x);
		print("div", div);
		print("flr", flr);
		if (x < 0) {
			x += (1 << 2) - 1;
		}
		int shft = x >> 2;
		print("x", x);
		print("shft", shft);
	}

	private static void example6() {
		print("Example 6");
		int x = 58;
		int result = ~x;
		print("x", x);
		print("~x", result);
	}

	private static void example7() {
		print("Example 7");
		int x = 58;
		int y = 107;
		int result = x & y;
		print("x", x);
		print("y", y);
		print("(x & y)", result);
	}

	private static void example8() {
		print("Example 8");
		int x = 58;
		int y = 107;
		int result = x | y;
		print("x", x);
		print("y", y);
		print("(x | y)", result);
	}

	private static void example9() {
		print("Example 9");
		int x = 58;
		int y = 107;
		int result = x ^ y;
		print("x", x);
		print("y", y);
		print("(x ^ y)", result);
	}

	private static void example10() {
		print("Example 10");
		int x = 58;
		print("x", x);
		x &= 0x0f;
		print("&= 0x0f", x);
		x |= 0x70;
		print("|= 0x70", x);
		x ^= 0xb7;
		print("^= 0xb7", x);
		x <<= 2;
		print("<<= 2", x);
		x >>>= 1;
		print(">>>= 1", x);
		x >>= 1;
		print(">>= 1", x);
	}

	private static void example11() {
		print("Example 11");
		int x = 58;
		print("x", x);
		x |= (1 << 2);  // Turn on bit #2
		print("1 << 2", 1 << 2);
		print("|= (1 << 2)", x);

		// Test if bit #2 is turned on
		print("x & (1 << 2)", x & (1 << 2));
		if ((x & (1 << 2)) != 0) {
			System.out.println("bit 2 is set");
		}
		else {
			System.out.println("bit 2 is clear");
		}

		print("1 << 3", 1 << 3);
		print("~(1 << 3)", ~(1 << 3));
		x &= ~(1 << 3);  // Turn off bit #3
		print("&= ~(1 << 3)", x);
	}

	private static void example12() {
		print("Example 12");
		int x = 58;
		int y = -42;
		print("x", x);
		print("y", y);
		x ^= y;
		print("x ^= y", x);
		y ^= x;
		print("y ^= x", y);
		x ^= y;
		print("x ^= y", x);
		print("x", x);
		print("y", y);
	}

	private static void example13() {
		print("Example 13");
		int x = -701;
		int y = x >> 31;
		int abs = (x + y) ^ y;
		print("x", x);
		print("y", y);
		print("(x + y)", (x + y));
		print("abs", abs);
	}

	private static void example14() {
		print("Example 14");
		int prefixLen = 20;
		int prefixMask = ((1 << prefixLen) - 1) << (32 - prefixLen);
		int sourceAddr = addrFromDotDec(192, 168, 54, 15);  // 0xc0a8360f
		int destAddr = addrFromDotDec(74, 125, 129, 99);  // 0x4a7d8163
		int sourcePrefix = sourceAddr & prefixMask;
		int destPrefix= destAddr & prefixMask;
		if (sourcePrefix != destPrefix) {
			/* Find a route for the packet.*/
		}
		print("prefixLen", prefixLen);
		print("prefixMask", prefixMask);
		System.out.println("prefixMask: " + dotDecFromAddr(prefixMask));
		print("sourceAddr", sourceAddr);
		print("sourcePrefix", sourcePrefix);
		System.out.println("sourceAddr: " + dotDecFromAddr(sourceAddr));
		System.out.println("sourcePrefix: " + dotDecFromAddr(sourcePrefix));
		print("destAddr", destAddr);
		print("destPrefix", destPrefix);
		System.out.println("destAddr: " + dotDecFromAddr(destAddr));
		System.out.println("destPrefix: " + dotDecFromAddr(destPrefix));
	}

	private static int addrFromDotDec(int w, int x, int y, int z) {
		return w * (1 << 24) + x * (1 << 16) + y * (1 << 8) + z;
	}

	private static String dotDecFromAddr(int addr) {
		StringBuilder sb = new StringBuilder(16);
		sb.append(Integer.toString(addr >>> 24)).append('.')
			.append(Integer.toString((addr >>> 16) & 0xff)).append('.')
			.append(Integer.toString((addr >>> 8) & 0xff)).append('.')
			.append(Integer.toString(addr & 0xff));
		return sb.toString();
	}

	private static void example15() {
		print("Example 15");
		int[] tests = {
			Integer.MIN_VALUE, Integer.MIN_VALUE + 1, Integer.MIN_VALUE + 2,
			Integer.MIN_VALUE / 2, Integer.MIN_VALUE / 2 + 1,
			2, -1, 0, 1, 2,
			Integer.MAX_VALUE / 2 - 1, Integer.MAX_VALUE / 2,
			Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE
		};
		long start = System.currentTimeMillis();
		for (int x : tests) {
			for (int y : tests) {
				int expected = x * y;
				int actual = mult(x, y);
				if (actual != expected) {
//					System.err.println("Error: " + x + " * " + y +
//							" expected: " + expected + ", but found: " + actual);
				}
			}
		}

		for (int x = -10000;  x <= 10000;  ++x) {
			for (int y = -10000;  y <= 10000;  ++y) {
				int expected = x * y;
				int actual = mult(x, y);
				if (actual != expected) {
					System.err.println("Error: " + x + " * " + y +
							" expected: " + expected + ", but found: " + actual);
				}
			}
		}
		long end = System.currentTimeMillis();
		long elapsed = end - start;
		System.out.println("elapsed seconds: " + elapsed / 1000.0);
	}

	private static void example16() {
		print("Example 16");
		{
			int x = 5;
			int y = 3;
			int sum = add(x, y);
			print("sum", sum);
		}
		{
			int x = 58;
			int y = -42;
			int sum = add(x, y);
			print("sum", sum);
		}
		Random rand = new Random();
		for (int i = 100000000;  i != 0;  --i) {
			int x = rand.nextInt();
			int y = rand.nextInt();
			int expected = x + y;
			int actual = add(x, y);
			if (actual != expected) {
				System.err.println("Error: " + x + " * " + y +
						" expected: " + expected + ", but found: " + actual);
			}
		}
	}

	public static int mult(int mcand, int mplier) {
		// Compute the sign of the product.
		int cs = mcand >> 31;
		int ps = mplier >> 31;
		int sign = cs ^ ps;

		// Make the multiplicand and
		// the multiplier non-negative.
		mcand = (mcand + cs) ^ cs;
		mplier = (mplier + ps) ^ ps;

		// If the multiplier is larger than the multiplicand,
		// then exchange their values.  We want the multiplier
		// to contain the smaller number so that the while
		// loop below repeats fewer times.
		if (mplier > mcand) {
			mplier ^= mcand;
			mcand ^= mplier;
			mplier ^= mcand;
		}

		// Multiply by using the "shift and add" algorithm.
		int prod = 0;
		while (mplier != 0) {
			int bit = mplier & 1;
//			int mask = ~bit + 1;
//			prod += mcand & mask;
			if (bit != 0) {
				prod += mcand;
			}
			mplier >>>= 1;
			mcand <<= 1;
		}

		// If the product should be negative then negate it.
		if (sign != 0) {
			prod = -prod;
		}
//		prod = tsign(sign, prod);

		return prod;
	}

	public static int sub(int x, int y) {
		return add(x, neg(y));
	}

	public static int neg(int x) {
		return add(~x, 1);
	}

	/** Transfers the sign of s onto x. */
	public static int tsign(int s, int x) {
		int t = (x ^ s) >> 31;
		return (x + t) ^ t;
	}

	public static int add(int x, int y) {
//		print("x", x);
//		print("y", y);
		int sum = x ^ y;
		int carry = x & y;
//		print("sum  ", sum);
//		print("carry", carry);
		while (carry != 0) {
			x = sum;
			y = carry << 1;
			sum = x ^ y;
			carry = x & y;
//			print("sum  ", sum);
//			print("carry", carry);
		}
		return sum;
	}

	public static int abs(int x) {
		// This algorithm to compute the absolute value
		// of a number comes from "Hacker's Delight" by
		// Henry S. Warren, Jr. section 2-4, page 17.
		int t = x >> 31;
		return (x + t) ^ t;
	}

	private static void example17() {
		print("Example 16");
		long plain = 0x0000737570657262L;  // ASCII values for "superb"
		long key = 0x36a1804be2f359e1L;
		long cipher = plain ^ key;    // encrypt
		long message = cipher ^ key;  // decrypt
		print("plain", plain);
		print("key", key);
		print("cipher", cipher);
		print("message", message);
	}


	private static void print(String label, int x) {
		System.out.println("    " + label +
				"\t" + x +
				"\t" + toHex(x) +
				"\t" + toBin(x));
	}

	private static void print(String label, long x) {
		System.out.println("    " + label +
				"\t" + toHex(x) +
				"\t" + toBin(x));
	}

	private static String toHex(int x) {
		String s = Long.toString(0xffffffffL & x, 16);
		int len = s.length();
		return "00000000".substring(len) + s;
	}

	private static String toBin(int x) {
		String s = Long.toString(0xffffffffL & x, 2);
		int len = s.length();
		s = "00000000000000000000000000000000".substring(len) + s;
		StringBuilder sb = new StringBuilder(s.substring(0, 4));
		for (int i = 4;  i < 32;  i += 4) {
			sb.append(' ').append(s.substring(i, i + 4));
		}
		return sb.toString();
	}

	private static String toHex(long x) {
		String s = Long.toString(x, 16);
		int len = s.length();
		s = "0000000000000000".substring(len) + s;
		return s.substring(0, 8) + ' ' + s.substring(8);
	}

	private static String toBin(long x) {
		String s = Long.toString(x, 2);
		int len = s.length();
		s = "0000000000000000000000000000000000000000000000000000000000000000"
			.substring(len) + s;
		StringBuilder sb = new StringBuilder(s.substring(0, 4));
		for (int i = 4;  i < 64;  i += 4) {
			sb.append(' ').append(s.substring(i, i + 4));
		}
		return sb.toString();
	}

	private static void print(String x) {
		System.out.println(x);
	}
}
