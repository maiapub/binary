import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;


public class Arith {
	@Test
	public void testArithmetic() {
		int x = 3;
		int n = -3;
		assertEquals(n, -x);
		assertEquals(n, ~x + 1);
		assertEquals(n, x * -1);
		assertEquals(n, x / -1);
		assertEquals(n, 0 - x);

		assertEquals(x, -n);
		assertEquals(x, ~n + 1);
		assertEquals(x, n * -1);
		assertEquals(x, n / -1);
		assertEquals(x, 0 - n);

		assertEquals(x, neg(n));
		assertEquals(x, abs(n));
		assertEquals(x, abs(x));
		assertEquals(n, neg(x));
//		for (int i = Integer.MIN_VALUE;  i < Integer.MAX_VALUE;  ++i) {
//			assertEquals(-i, neg(i));
//			assertEquals(Math.abs(i), abs(i));
//		}
//		int i = Integer.MAX_VALUE;
//		assertEquals(-i, neg(i));
//		assertEquals(Math.abs(i), abs(i));

//		int[] a = new int[1];
//		int min = Integer.MIN_VALUE;
//		int max = Integer.MAX_VALUE;
//		long start = System.currentTimeMillis();
//		int i;
//		for (i = min;  i < max;  ++i) {
//			a[0] = -i;
//		}
//		long elapsed1 = System.currentTimeMillis() - start;
//		start = System.currentTimeMillis();
//		for (i = min;  i < max;  ++i) {
//			a[0] = ~i + 1;
//		}
//		long elapsed2 = System.currentTimeMillis() - start;
//		System.out.println("- " + elapsed1 + "  neg " + elapsed2);

//		start = System.currentTimeMillis();
//		for (i = min;  i < max;  ++i) {
//			a[0] = i < 0 ? -i : i;
//		}
//		elapsed1 = System.currentTimeMillis() - start;
//		int t;
//		start = System.currentTimeMillis();
//		for (i = min;  i < max;  ++i) {
//			t = x >> 31;
//			a[0] = (x + t) ^ t;
//		}
//		elapsed2 = System.currentTimeMillis() - start;
//		System.out.println("Math.abs " + elapsed1 + "  abs " + elapsed2);

		System.out.println(Math.abs(Integer.MIN_VALUE));
		System.out.println(-Math.abs(Integer.MIN_VALUE));
		System.out.println(nabs(Integer.MIN_VALUE));

		assertEquals(multByAdd(-7, 21), -7*21);
		assertEquals(divBySub(-21, 7), 21/-7);
		assertEquals(divBySub(-7, 21), -7/21);
	}


	@Test
	public void testMultiplication() {
		int[] a = { 0, 0, 1, 1, 0, -1, 57, 90, 63, -68, -18, 25, -70, -85, -60, 60, -60, Integer.MAX_VALUE / 2, 3 };
		int[] b = { 0, 1, 0, 1, -1, 0, 90, 57, -68, 63, 25, -18, -85, -70, 60, -60, -60, 2, Integer.MIN_VALUE / 3 };
		for (int i = 0;  i < a.length;  ++i) {
			int p0 = a[i] * b[i];
			int p1 = mult1(a[i], b[i]);
			int p2 = mult2(a[i], b[i]);
			int p3 = mult3(a[i], b[i]);
			assertEquals(p0, p1);
			assertEquals(p0, p2);
			assertEquals(p0, p3);
		}

		Random rand = new Random();
		int limit = (int)1e7;
		for (int i = 0;  i < limit;  ++i) {
//			int x = -46340 + rand.nextInt(92681);
//			int y = -46340 + rand.nextInt(92681);
			int x = rand.nextInt();
			int y = rand.nextInt();
			int p0 = x * y;
			int p1 = mult1(x, y);
			int p2 = mult2(x, y);
			int p3 = mult3(x, y);
			assertEquals(p0, p1);
			assertEquals(p0, p2);
			assertEquals(p0, p3);
		}

		limit = (int)1e6;
		a = new int[limit];
		b = new int[limit];
		rand = new Random(-75613);
		for (int i = 0;  i < a.length;  ++i) {
			a[i] = -46340 + rand.nextInt(92681);
			b[i] = -46340 + rand.nextInt(92681);
		}

		int[][] prods = new int[4][limit];
		long[] elapsed = new long[4];
		limit = 100;
		int[] p = prods[0];
		long start = System.currentTimeMillis();
		for (int t = 0;  t < limit;  ++t) {
			for (int i = 0;  i < a.length;  ++i) {
				p[i] = a[i] * b[i];
			}
		}
		elapsed[0] = System.currentTimeMillis() - start;

		p = prods[1];
		start = System.currentTimeMillis();
		for (int t = 0;  t < limit;  ++t) {
			for (int i = 0;  i < a.length;  ++i) {
				p[i] = mult1(a[i], b[i]);
			}
		}
		elapsed[1] = System.currentTimeMillis() - start;

		p = prods[2];
		start = System.currentTimeMillis();
		for (int t = 0;  t < limit;  ++t) {
			for (int i = 0;  i < a.length;  ++i) {
				p[i] = mult2(a[i], b[i]);
			}
		}
		elapsed[2] = System.currentTimeMillis() - start;

		p = prods[3];
		start = System.currentTimeMillis();
		for (int t = 0;  t < limit;  ++t) {
			for (int i = 0;  i < a.length;  ++i) {
				p[i] = mult3(a[i], b[i]);
			}
		}
		elapsed[3] = System.currentTimeMillis() - start;

		assertTrue(Arrays.equals(prods[0], prods[1]));
		assertTrue(Arrays.equals(prods[0], prods[2]));
		assertTrue(Arrays.equals(prods[0], prods[3]));
		System.out.println("multiplication times " + Arrays.toString(elapsed));
	}


	public static int neg(int x) {
		return ~x + 1;
	}

	public static int abs(int x) {
		int t = x >> 31;
			return (x + t) ^ t;
	}

	public static int nabs(int x) {
		return x < 0 ? x : -x;
	}

	public static int tsign(int x, int y) {
		int t = (x ^ y) >> 31;
		return (x + t) ^ t;
	}

	public static int sub(int x, int y) {
		return x + neg(y);
	}

	public static int clearLeast(int x) {
		return x & (x - 1);
	}

	public static int getLeastSigBit(int x) {
		return x & ~(x - 1);
	}

	public static int getMostSigBit(int x) {
		x = fold(x);
		return x & ~(x >> 1);
	}

	/** Sets all bits to the right of
	 * the most significant set bit. */
	public static int fold(int x) {
		x |= (x >> 1);
		x |= (x >> 2);
		x |= (x >> 4);
		x |= (x >> 8);
		return x | (x >> 16);
	}

	public static int popCount(int x) {
		x = x - ((x >>> 1) & 0x55555555);
		x = (x & 0x33333333) + ((x >>> 2) & 0x33333333);
		x = (x + (x >>> 4)) & 0x0f0f0f0f;
		x = x + (x >>> 8);
		x = x + (x >>> 16);
		return x & 0xff;
	}


	public static int mult2(int mcand, int mplier) {
		// Compute the sign of the product.
		int cs = mcand >> 31;
		int ps = mplier >> 31;
		int sign = cs ^ ps;

		// Make the multiplicand and
		// the multiplier non-negative.
		mcand = (mcand + cs) ^ cs;
		mplier = (mplier + ps) ^ ps;

		// If the multiplicand is smaller than the multiplier,
		// then exchange their values.  We want the multiplier
		// to contain the smaller number so that the while
		// loop below repeats fewer times.
		if (mplier > mcand) {
			mplier ^= mcand;
			mcand ^= mplier;
			mplier ^= mcand;
		}

		// Multiply by using the "shift and add" algorithm.
		int prod = 0;
		while (mplier != 0) {
			int bit = mplier & 1;
			if (bit != 0) {
				prod += mcand;
			}
			mplier >>>= 1;
			mcand <<= 1;
		}

		// If the product should be negative then negate it.
		if (sign != 0) {
			prod = -prod;
		}

		return prod;
	}


	public static int mult1(int x, int y) {
		int signMask = 0x80000000;
		//int sign = (x & signMask) ^ (y & signMask);
		int sign;
		{
			int xs = x & signMask;
			int ys = y & signMask;
			if (xs != 0) {
				x = ~x + 1;
			}
			if (ys != 0) {
				y = ~y + 1;
			}
			sign = xs ^ ys;
		}

		if (x > y) {
			int swap = x;
			x = y;
			y = swap;
		}

		int prod = 0;
		if (x != 0) {
			while (x != 0) {
				int twoBits = x & 0x3;
				switch (twoBits) {
				case 3:  prod += y;  /* fall through */
				case 2:  prod += y << 1;  break;
				case 1:  prod += y;  /* fall through */
				default: break;
				}
				x >>>= 2;
				y <<= 2;
			}

			if (sign != 0) {
				prod = ~prod + 1;
			}
		}
		return prod;
	}


	public static int mult3(int x, int y) {
		int prod = 0;
		while (x != 0) {
			int oneBit = x & 1;
			if (oneBit != 0) {
				prod += y;
			}
			x >>>= 1;
			y <<= 1;
		}
		return prod;
	}


	public static int multByAdd(int mcand, int mplier) {
		// Compute the sign of the product.
		boolean negative = (mcand < 0) != (mplier < 0);

		// Make the multiplicand and
		// the multiplier non-negative.
		mcand = Math.abs(mcand);
		mplier = Math.abs(mplier);

		// If the multiplier is larger than the multiplicand,
		// then exchange their values.  We want the multiplier
		// to contain the smaller number so that the while
		// loop below repeats fewer times.
		if (mplier > mcand) {
			int swap = mcand;
			mcand = mplier;
			mplier = swap;
		}

		int prod = 0;
		while (mplier != 0) {
			prod += mcand;
			mplier--;
		}

		// If the product should be negative then negate it.
		if (negative) {
			prod = -prod;
		}

		return prod;
	}


	public static int divBySub(int dend, int dsor) {
		int quot = 0;
		if (dsor == 0) {
			throw new ArithmeticException("divide by 0");
		}
		else if (dend == 0x80000000 && dsor == -1) {
			throw new ArithmeticException("overflow");
		}
		else {
			// Compute the sign of the quotient.
			boolean negative = (dend < 0) != (dsor < 0);

			// Make the dividend and the divisor non-negative.
			dend = Math.abs(dend);
			dsor = Math.abs(dsor);

			if (dsor == 1) {
				quot = dend;
			}
			else {
				// Compute the quotient.
				while (dend >= dsor) {
					dend = dend - dsor;
					quot++;
				}
			}

			// If the quotient should be negative then negate it.
			if (negative) {
				quot = -quot;
			}
		}
		return quot;
	}
}
