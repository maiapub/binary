#include "verify.h"

#include <stdlib.h>  /* abs(), exit() */
#include <stdio.h>   /* fprintf() */

void assertFunc(const char *file, int line, const char *test) {
	fprintf(stderr, "assertion failure: %s, line %d: \"%s\"\n",
			file, line, test);
	exit(1);
}


bool fltEq(float x, float y, float diff) {
	return abs(x - y) <= diff;
}

bool dblEq(double x, double y, double diff) {
	return abs(x - y) <= diff;
}
