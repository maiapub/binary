/* gcc -O -std=c99 -Wall -o arith arith.c verify.c */

#include "verify.h"

#include <limits.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


/** A library of simple arithmetic functions that are entirely
 * implemented with only left shift, right shift, not, and, or, xor. */

int add(int x, int y) {
	int sum = x ^ y;
	int carry = x & y;
	while (carry != 0) {
		x = sum;
		y = carry << 1;
		sum = x ^ y;
		carry = x & y;
	}
	return sum;
}

int sign(int x) {
	return x & 0x80000000;
}

int neg(int x) {
	return add(~x, 1);
}

unsigned int babs(int x) {
	// This algorithm to compute the absolute value
	// of a number comes from "Hacker's Delight" by
	// Henry S. Warren, Jr. section 2-4, page 17.
	int t = x >> 31;
	return add(x, t) ^ t;
}

/** Transfers the sign of s onto x. */
static int tsign(int s, int x) {
	int t = (x ^ s) >> 31;
	return add(x, t) ^ t;
}

int sub(int x, int y) {
	return add(x, neg(y));
}


bool eq(int x, int y) {
	return sub(x, y) == 0;
}

bool lt(int x, int y) {
	return sign(sub(x, y)) != 0;
}

bool gt(int x, int y) {
	return sign(sub(y, x)) != 0;
}

bool lteq(int x, int y) {
	return sign(sub(y, x)) == 0;
}

bool gteq(int x, int y) {
	return sign(sub(x, y)) == 0;
}


static int clearLeast(int x) {
	return x & sub(x, 1);
}

static int leastSigBit(int x) {
	return x & ~sub(x, 1);
}

/** Sets all bits to the right of
 * the most significant set bit. */
static int fold(int x) {
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	return x | (x >> 16);
}

static int mostSigBit(int x) {
	x = fold(x);
	return x & ~(x >> 1);
}


int mult(int mcand, int mplier) {
	// Compute the sign of the product.
	int cs = mcand >> 31;
	int ps = mplier >> 31;
	int s = cs ^ ps;

	// Make the multiplicand and
	// the multiplier non-negative.
	mcand = add(mcand, cs) ^ cs;
	mplier = add(mplier, ps) ^ ps;

	// If the multiplier is larger than the multiplicand,
	// then exchange their values.  We want the multiplier
	// to contain the smaller number so that the while
	// loop below repeats fewer times.
	if (gt(mplier, mcand)) {
		mplier ^= mcand;
		mcand ^= mplier;
		mplier ^= mcand;
	}

	// Multiply by using the "shift and add" algorithm.
	unsigned int mp = (unsigned int)mplier;
	int prod = 0;
	while (mp != 0) {
		int bit = mp & 1;
//		if (bit != 0) {
//			prod = add(prod, mcand);
//		}
		int mask = ~bit + 1;
		prod += mcand & mask;
		mp >>= 1;
		mcand <<= 1;
	}

	// If the product should be negative then negate it.
	if (s != 0) {
		prod = neg(prod);
	}
//		prod = tsign(sign, prod);

	return prod;
}


int bdiv(int dend, int dsor) {
	int quot = 0;
	if (dsor == 0) {
	}
	else if (eq(dend, 0x80000000) && eq(dsor, -1)) {
	}
	else {
		// Compute the sign of the quotient.
		int cs = dend >> 31;
		int ps = dsor >> 31;
		int s = cs ^ ps;

		// Make the dividend and
		// the divisor non-negative.
		dend = add(dend, cs) ^ cs;
		dsor = add(dsor, ps) ^ ps;

		if (eq(dsor, 1)) {
			quot = dend;
		}
		else {
			// Compute the quotient.
			while (gteq(dend, dsor)) {
				dend = sub(dend, dsor);
				quot = add(quot, 1);
			}
		}

		if (s != 0) {
			quot = neg(quot);
		}
	}
	return quot;
}


static int brem(int dend, int dsor) {
	return 0;
}


static void testRightShift() {
	int pos = 0x7fffffff;
	int neg = 0x80000000;
	assert(0x3fffffff == pos >> 1);
	assert(0xc0000000 == neg >> 1);
	assert(0x3fffffff == (unsigned)pos >> 1);
	assert(0x40000000 == (unsigned)neg >> 1);
}


static void testArith() {
	int x = 3;
	int n = -3;
	assert(n == -x);
	assert(n == ~x + 1);
	assert(n == x * -1);
	assert(n == x / -1);
	assert(n == 0 - x);

	assert(x == -n);
	assert(x == ~n + 1);
	assert(x == n * -1);
	assert(x == n / -1);
	assert(x == 0 - n);

	assert(x == neg(n));
	assert(x == abs(n));
	assert(x == abs(x));
	assert(n == neg(x));
	//		for (int i = INT_MIN;  i < INT_MAX;  ++i) {
	//			assertEq(-i, neg(i));
	//			assertEq(Math.abs(i), abs(i));
	//		}
	//		int i = INT_MAX;
	//		assertEq(-i, neg(i));
	//		assertEq(Math.abs(i), abs(i));

	int dend = 15;
	int dsor = 4;
	printf("%d / %d = %d\n", dend, dsor, dend / dsor);
	dend = -dend;
	printf("%d / %d = %d\n", dend, dsor, dend / dsor);
	dsor = -dsor;
	dend = -dend;
	printf("%d / %d = %d\n", dend, dsor, dend / dsor);
	dend = -dend;
	printf("%d / %d = %d\n", dend, dsor, dend / dsor);

	//		int[] a = new int[1];
	//		int min = INT_MIN;
	//		int max = INT_MAX;
	//		long start = System.currentTimeMillis();
	//		int i;
	//		for (i = min;  i < max;  ++i) {
	//			a[0] = -i;
	//		}
	//		long elapsed1 = System.currentTimeMillis() - start;
	//		start = System.currentTimeMillis();
	//		for (i = min;  i < max;  ++i) {
	//			a[0] = ~i + 1;
	//		}
	//		long elapsed2 = System.currentTimeMillis() - start;
	//		System.out.println("- " + elapsed1 + "  neg " + elapsed2);
	//
	//		start = System.currentTimeMillis();
	//		for (i = min;  i < max;  ++i) {
	//			a[0] = i < 0 ? -i : i;
	//		}
	//		elapsed1 = System.currentTimeMillis() - start;
	//		int t;
	//		start = System.currentTimeMillis();
	//		for (i = min;  i < max;  ++i) {
	//			t = x >> 31;
	//			a[0] = (x + t) ^ t;
	//		}
	//		elapsed2 = System.currentTimeMillis() - start;
	//		System.out.println("Math.abs " + elapsed1 + "  abs " + elapsed2);

	//System.out.println(Math.abs(INT_MIN));
	//System.out.println(-Math.abs(INT_MIN));
	//System.out.println(nabs(INT_MIN));
}


static void ellapsed(const struct timeval *start, const struct timeval *end) {
	long TICKS = 1000000;
	double secs = ((end->tv_sec * TICKS + end->tv_usec) -
			(start->tv_sec * TICKS + start->tv_usec)) / (double)TICKS;
	printf("  elapsed seconds: %f\n", secs);
}


static int length = 1e3;
static int *rands;

static void fillRands() {
	struct timeval start, end;
	printf("testDiv()\n");
	gettimeofday(&start, NULL);
	rands = malloc(sizeof(rands[0]) * length);
	for (int i = 0;  i < length;  ++i) {
		rands[i] = (int)mrand48();
		printf("%d\n", rands[i]);
	}
	gettimeofday(&end, NULL);
	ellapsed(&start, &end);
}


static void testMult() {
	static const int a[] = { 0, 0, 1, 1, 0, -1, 57, 90, 63, -68, -18, 25, -70, -85, -60, 60, -60, INT_MAX / 2, 3 };
	static const int b[] = { 0, 1, 0, 1, -1, 0, 90, 57, -68, 63, 25, -18, -85, -70, 60, -60, -60, 2, INT_MIN / 3 };

	struct timeval start, end;
	printf("testMult()\n");

	gettimeofday(&start, NULL);
	for (int i = 0;  i < CAP(a);  ++i) {
		int p0 = a[i] * b[i];
		int p1 = mult(a[i], b[i]);
		assert(p0 == p1);
	}

	int limit = 1;
	for (int i = limit;  i;  --i) {
		for (int j = 0;  j < length;  ++j) {
			int x = rands[j];
			for (int k = 0;  k < length;  ++k) {
				int y = rands[k];
				int p0 = x * y;
				int p1 = mult(x, y);
				assert(p0 == p1);
			}
		}
	}
	gettimeofday(&end, NULL);
	ellapsed(&start, &end);

	/*
	limit = (int)1e6;
	a = new int[limit];
	b = new int[limit];
	rand = new Random(-75613);
	for (int i = 0;  i < a.length;  ++i) {
		a[i] = -46340 + rand.nextInt(92681);
		b[i] = -46340 + rand.nextInt(92681);
	}

	int[][] prods = new int[4][limit];
	long[] elapsed = new long[4];
	limit = 100;
	int[] p = prods[0];
	long start = System.currentTimeMillis();
	for (int t = 0;  t < limit;  ++t) {
		for (int i = 0;  i < a.length;  ++i) {
			p[i] = a[i] * b[i];
		}
	}
	elapsed[0] = System.currentTimeMillis() - start;

	p = prods[1];
	start = System.currentTimeMillis();
	for (int t = 0;  t < limit;  ++t) {
		for (int i = 0;  i < a.length;  ++i) {
			p[i] = mult1(a[i], b[i]);
		}
	}
	elapsed[1] = System.currentTimeMillis() - start;

	p = prods[2];
	start = System.currentTimeMillis();
	for (int t = 0;  t < limit;  ++t) {
		for (int i = 0;  i < a.length;  ++i) {
			p[i] = mult2(a[i], b[i]);
		}
	}
	elapsed[2] = System.currentTimeMillis() - start;

	p = prods[3];
	start = System.currentTimeMillis();
	for (int t = 0;  t < limit;  ++t) {
		for (int i = 0;  i < a.length;  ++i) {
			p[i] = mult3(a[i], b[i]);
		}
	}
	elapsed[3] = System.currentTimeMillis() - start;

	assertTrue(Arrays.equals(prods[0], prods[1]));
	assertTrue(Arrays.equals(prods[0], prods[2]));
	assertTrue(Arrays.equals(prods[0], prods[3]));
	System.out.println("multiplication times " + Arrays.toString(elapsed));
	*/
}


static void testDiv() {
	struct timeval start, end;
	printf("testDiv()\n");
	gettimeofday(&start, NULL);
	int limit = 1;
	for (int i = limit;  i;  --i) {
		for (int j = 0;  j < length;  ++j) {
			int x = rands[j];
			for (int k = 0;  k < length;  ++k) {
				while (k < length && rands[k] == 0) {
					++k;
				}
				int y = rands[k];
				int p0 = x / y;
				int p1 = bdiv(x, y);
				assert(p0 == p1);
			}
		}
	}
	gettimeofday(&end, NULL);
	ellapsed(&start, &end);
}


int main(int argc, char *const argv[]) {
	testRightShift();
	testArith();
	fillRands();
	testMult();
	testDiv();
	free(rands);
	return 0;
}
