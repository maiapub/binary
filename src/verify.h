#ifndef VERIFY_H
#define VERIFY_H

#include <stdbool.h>

/* Computes the capacity of an array.  This is the number of elements
 * the array can hold, not the number of bytes in the array.  In order
 * for this macro to work, arr must be an array (e.g. int arr[10])
 * and not a pointer (e.g. int *arr) within the current scope. */
#define CAP(arr) (sizeof(arr) / sizeof(arr[0]))

/* Provide our own assert macro and function that
 * doesn't cause a core dump when it fails. */
#define assert(e) ((e) ? (void)0 : assertFunc(__FILE__, __LINE__, #e))

extern void assertFunc(const char *file, int line, const char *test);

extern bool fltEq(float x, float y, float diff);
extern bool dblEq(double x, double y, double diff);

#endif
