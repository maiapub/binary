#!/bin/python3

def testIntDiv():
	dend = 15
	dsor = 4
	print(str(dend) + ' // ' + str(dsor) + ' = ' + str(dend // dsor))
	dsor = -dsor
	print(str(dend) + ' // ' + str(dsor) + ' = ' + str(dend // dsor))
	dend = -dend
	dsor = -dsor
	print(str(dend) + ' // ' + str(dsor) + ' = ' + str(dend // dsor))
	dsor = -dsor
	print(str(dend) + ' // ' + str(dsor) + ' = ' + str(dend // dsor))

def testUnsignedRightShift():
	dend = 15
	dsor = 2
	print('abs(' + str(dend) + ') >> ' + str(dsor) + ' = ' +
		str(abs(dend) >> dsor))
	dend = -dend
	print('abs(' + str(dend) + ') >> ' + str(dsor) + ' = ' +
		str(abs(dend) >> dsor))

def testSignedRightShift():
	dend = 15
	dsor = 2
	print(str(dend) + ' >> ' + str(dsor) + ' = ' + str(dend >> dsor))
	dend = -dend
	print(str(dend) + ' >> ' + str(dsor) + ' = ' + str(dend >> dsor))

def testArith():
	testIntDiv()
	testUnsignedRightShift()
	testSignedRightShift()

testArith()
