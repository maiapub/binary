#!/usr/bin/python3

def display(label, x):
	print('    ' + label +
			'\t' + str(x) +
			'\t' + hex(0xffffffff & x) +
			'\t' + bin(0xffffffff & x))

def example1():
	print('Example 1')
	x = 53
	pos = x << 2
	z = -42
	neg = z << 2
	display('x', x)
	display('pos', pos)
	display('z', z)
	display('neg', neg)

#def example2():
#	print('Example 2')
#	x = 53
#	pos = x >>> 2
#	z = -42
#	neg = z >>> 2
#	display('x', x)
#	display('pos', pos)
#	display('z', z)
#	display('neg', neg)

def example3():
	print('Example 3')
	x = 53
	pos = x >> 2
	display('x', x)
	display('pos', pos)

def example4():
	print('Example 4')
	w = -40
	x = -41
	y = -42
	z = -43
	srsw = w >> 2
	srsx = x >> 2
	srsy = y >> 2
	srsz = z >> 2
	display('w', w)
	display('x', x)
	display('y', y)
	display('z', z)
	display('srsw', srsw)
	display('srsx', srsx)
	display('srsy', srsy)
	display('srsz', srsz)

def example5():
	print('Example 5')
	x = -43
	div = x // pow(2, 2)
	display('x', x)
	display('div', div)
	if x < 0:
		x += (1 << 2) - 1
	shft = x >> 2
	display('x', x)
	display('shft', shft)

def example6():
	print('Example 6')
	x = 53
	result = ~x
	display('x', x)
	display('result', result)

example1()
#example2()
example3()
example4()
example5()
example6()
